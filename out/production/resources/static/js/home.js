$(document).ready(function(){

    $('.loginBtn').unbind('click').click(function() {
        $('#loginModal').modal();
    });

    $('.modal-text-field').on('focus', function(){
        $(this).removeAttr('placeholder');
    });

    $('.modal-text-field').on('focusout', function(){
        var $this = $(this);
        var name = $this.attr('id');
        if($this.val() == ""){
            name == "username" ? $this.attr('placeholder','Username') : $this.attr('placeholder','Password');
        }
    });

    //TODO
    //tabbing into 'login' field in modal does not trigger css styling
    $('#modal-login-btn').on('focus', function(){
        $(this).trigger('mouseenter');
    })

});